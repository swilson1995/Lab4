package ie.ucd.partyPeople;

import ie.ucd.items.AlcoholicDrink;
import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.people.Person;

public class NotDrinker extends Person {

	/**
	 * Constructer for the NotDrinker
	 * NonDrinker cannot intake alcoholic drinks 
	 * @param Weigth is the weight of the NotDrinkerr
	 */
	public NotDrinker(double Weigth) {
		this.setWeight(Weigth);
	}
	
	/**
	 * Method for NotDrinker to intake a drink.
	 * @param arg0 a Drink object to bee drunk
	 * @return true if the drink is non-alcoholic, false if alcoholic
	 */
	@Override
	public boolean drink(Drink arg0) {
		if(arg0 instanceof AlcoholicDrink) {
			
			return false;
		}
		else {
			return true;
		}
	}

	/**
	 * Method for a NotDrinker to intake food
	 * @param arg0 The food object to be eaten
	 * @return always return true. There is never an issue with eating
	 */
	@Override
	public boolean eat(Food arg0) {
		
		return true;
	}

}
