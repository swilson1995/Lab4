package ie.ucd.partyPeople;

import java.util.ArrayList;
import java.util.List;

import ie.ucd.items.AlcoholicDrink;
import ie.ucd.items.NotAlcoholicDrink;
import ie.ucd.partyDrinks.Vodka;
import ie.ucd.partyDrinks.Water;
import ie.ucd.people.Person;

public class Main {

	public static void main(String[] args) {
		/*
		 * Create a few people, some drinkers and some non drinkers
		 * Add them to a list
		 */
		Person tim =new NotDrinker(100);
		Person tom=new Drinker(50);
		Person tam=new Drinker(70);
		List<Person> allPeople=new ArrayList<Person>();
		allPeople.add(tim);
		allPeople.add(tom);
		allPeople.add(tam);
		
		boolean someoneIsDrunk=false;
		/*
		 * Now, in a loop, add drinks to each person until someone becomes drunk, 
		 * and the party ends
		 */
		do {
			for (Person person : allPeople) {
				if(person instanceof NotDrinker) {
					NotAlcoholicDrink water=new Water("water", 200);
					person.drink(water);
				}
				else if(person instanceof Drinker) {
					AlcoholicDrink vodka=new Vodka("Tesco Value", 100, 20);
					person.drink(vodka);
					if(((Drinker) person).isDrunk()) {
						someoneIsDrunk=true;

					}
				}
			}
		}while(!someoneIsDrunk);
		
		/*
		 * Find out how much each Drinker person drank 
		 */
		int index=0;
		for (Person person : allPeople) {
			
			if(person instanceof Drinker) {
				Drinker drinker=(Drinker) person;
				System.out.println("Person "+index+" has taken "+drinker.getNumberOfDrinks());
				if(drinker.isDrunk()) {
					System.out.println("This person got drunk");
				}
			}
			index++;
		}
		
	}

}
