package ie.ucd.partyPeople;

import ie.ucd.items.AlcoholicDrink;
import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.people.Person;

public class Drinker extends Person {
	
	private int numberOfDrinks;
	
	public Drinker(double Weigth) {
		numberOfDrinks=0;
		super.setWeight(Weigth);
		
	}
	
	/**
	 * Method for Drinker to intake a drink. If the drink is alcoholic, the number of drinks
	 * the Drinker drank increases by 1
	 * @param arg0 a Drink object to bee drunk
	 * @return true 
	 */
	@Override
	public boolean drink(Drink arg0) {
		if(arg0 instanceof AlcoholicDrink) {
			this.numberOfDrinks++;
		}
		return true;
	}

	/**
	 * Method for a Drinker to intake food
	 * @param arg0 The food object to be eaten
	 * @return always return true. There is never an issue with eating
	 */
	@Override
	public boolean eat(Food arg0) {
		return true;
	}
	/**
	 * This method checks if  the person has  drunk enough to become drunk
	 * @return
	 */
	public boolean isDrunk() {
		if(numberOfDrinks>this.getWeight()/10) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Get the number of drinks the individual has taken
	 * @return number of drinks taken
	 */
	public int getNumberOfDrinks() {
		return numberOfDrinks;
	}

}
