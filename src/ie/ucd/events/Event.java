package ie.ucd.events;

import java.util.*;
import ie.ucd.people.Person;

public abstract class Event {
	private ArrayList<Person> parIcipants;

	public Event(ArrayList<Person> parIcipants) {
		this.parIcipants = parIcipants;
	}

	public abstract boolean isSuccessful();

	public ArrayList<Person> getParIcipants() {
		return parIcipants;
	}

	public void setParIcipants(ArrayList<Person> parIcipants) {
		this.parIcipants = parIcipants;
	}
}
