package ie.ucd.partyDrinks;

import ie.ucd.items.AlcoholicDrink;

public class Vodka extends AlcoholicDrink {
	/**
	 * This is to test the Drinker person class objects
	 * @param name
	 * @param volume
	 * @param degrees
	 */
	public Vodka(String name, double volume, double degrees) {
		super(name, volume, degrees);
		
	}

}
