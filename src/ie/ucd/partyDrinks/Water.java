package ie.ucd.partyDrinks;

import ie.ucd.items.NotAlcoholicDrink;

public class Water extends NotAlcoholicDrink {
	/**
	 * This is to be used to test the non drinker class objects
	 * @param name Water
	 * @param volume How much water
	 */
	public Water(String name, double volume) {
		super(name, volume);
		
	}

}
